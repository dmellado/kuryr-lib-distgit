%{!?upstream_version: %global upstream_version %{version}%{?milestone}}

%global project kuryr
%global library kuryr-lib
%global module kuryr_lib

%if 0%{fedora} >= 24
%global with_python3 1
%endif

Name: python-%library
Version: XXX
Release: XXX
Summary: OpenStack Kuryr library
License:    ASL 2.0
URL:        http://docs.openstack.org/developer/kuryr

Source0:    https://tarballs.openstack.org/%{project}/%{library}-%{upstream_version}.tar.gz

BuildArch: noarch

%package -n python2-%{library}
Summary: OpenStack Kuryr library
%{?python_provide:%python_provide python2-%{library}}


BuildRequires:  git
BuildRequires:  python-pbr
BuildRequires:  python2-devel
BuildRequires:  python-setuptools

Requires:       python-ipaddress >= 1.0.7
Requires:       python-keystonauth1 >= 2.18.0
Requires:       python-neutronclient >= 5.1.0
Requires:       python-oslo-concurrency >= 3.8.0
Requires:       python-oslo-config >= 2:3.14.0
Requires:       python-oslo-i18n >= 2.1.0
Requires:       python-oslo-log >= 3.11.0
Requires:       python-oslo-utils >= 3.18.0
Requires:       python-pyroute2 >= 0.4.13
Requires:       python-six >= 1.9.0

%description -n python2-%{library}
OpenStack Kuryr library shared by all Kuryr sub-projects.

%package -n python2-%{library}-tests
Summary:    OpenStack Kuryr library tests
Requires:   python2-%{library} = %{version}-%{release}

%description -n python2-%{library}-tests
OpenStack Kuryr library shared by all Kuryr sub-projects.

This package contains the Kuryr library test files.

%package doc
Summary:    OpenStack Kuryr library documentation

BuildRequires: python-sphinx
BuildRequires: python-oslo-sphinx

%description doc
OpenStack Kuryr library shared by all Kuryr sub-projects.

This package contains the documentation.

%if 0%{?with_python3}
%package -n python3-%{library}
Summary: OpenStack Kuryr library
%{?python_provide:%python_provide python3-%{library}}

BuildRequires:  python3-devel
BuildRequires:  python3-pbr
BuildRequires:  python3-setuptools
BuildRequires:  git

Requires:       python3-ipaddress >= 1.0.7
Requires:       python3-keystonauth1 >= 2.18.0
Requires:       python3-neutronclient >= 5.1.0
Requires:       python3-oslo-concurrency >= 3.8.0
Requires:       python3-oslo-config >= 2:3.14.0
Requires:       python3-oslo-i18n >= 2.1.0
Requires:       python3-oslo-log >= 3.11.0
Requires:       python3-oslo-utils >= 3.18.0
Requires:       python3-pyroute2 >= 0.4.13
Requires:       python3-six >= 1.9.0

%description -n python3-%{library}
OpenStack Kuryr library shared by all Kuryr sub-projects

This package contains the Python3 version of the library

%package -n python3-%{library}-tests
Summary:    OpenStack Kuryr library tests
Requires:   python3-%{library} = %{version}-%{release}

%description -n python3-%{library}-tests
OpenStack example library.

This package contains the example library test files.

%endif # with_python3


%description
OpenStack Kuryr library shared by all Kuryr sub-projects.

%prep
%autosetup -n %{library}-%{upstream_version} -S git

# Let's handle dependencies ourseleves
rm -f requirements.txt

%build
%py2_build
%if 0%{?with_python3}
%py3_build
%endif

# generate html docs
sphinx-build doc/source html
# remove the sphinx-build leftovers
rm -rf html/.{doctrees,buildinfo}

%install
%py2_install
%if 0%{?with_python3}
%py3_install
%endif

%check
%{__python2} setup.py test
%if 0%{?with_python3}
%{__python3} setup.py test
rm -rf .testrepository
%endif

%files -n python2-{%library}
%license LICENSE
%{python2_sitelib}/%{module}
%{python2_sitelib}/%{module}-*.egg-info
%exclude %{python2_sitelib}/%{module}/tests

%files -n python2-%{library}-tests
%license LICENSE
%{python2_sitelib}/%{module}/tests

%files doc
%license LICENSE
%doc html README.rst

%if 0%{?with_python3}
%files python3-%{library}
%license LICENSE
%{python3_sitelib}/%{module}
%{python3_sitelib}/%{module}-*.egg-info
%exclude %{python3_sitelib}/%{module}/tests

%files -n python3-%{library}-tests
%license LICENSE
%{python3_sitelib}/%{module}/tests
%endif

%changelog
